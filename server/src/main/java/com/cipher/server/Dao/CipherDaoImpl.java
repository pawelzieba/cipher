package com.cipher.server.Dao;


import com.cipher.server.Entity.TextEntity;

import org.springframework.stereotype.Repository;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.Scanner;

@Repository
public class CipherDaoImpl implements CipherDao {

    private int shiftAmount;

    public CipherDaoImpl() {
        shiftAmount = readShiftAmount();
    }

    public TextEntity encode(String s) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < s.length(); i++) {
            stringBuilder.append(codeChar(s.charAt(i)));
        }
        return new TextEntity(stringBuilder.toString());
    }

    public TextEntity decode(String s) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < s.length(); i++) {
            stringBuilder.append(decodeChar(s.charAt(i)));
        }
        return new TextEntity(stringBuilder.toString());
    }

    private char codeChar(char c) {
        if(c >= 65 && c<= 90) {
            c -= 65;
            c += shiftAmount;
            c %= 26;
            c += 65;
        } else if (c >= 97 && c<= 122) {
            c -= 97;
            c += shiftAmount;
            c %= 26;
            c += 97;
        }
        return c;
    }

    private char decodeChar(char c) {
        if(c >= 65 && c<= 90) {
            c -= 65;
            c -= shiftAmount;
            c += 26;
            c %= 26;
            c += 65;
        } else if (c >= 97 && c<= 122) {
            c -= 97;
            c -= shiftAmount;
            c += 26;
            c %= 26;
            c += 97;
        }
        return c;
    }

    private File loadShiftAmount()
            throws FileNotFoundException {
        return ResourceUtils.getFile(
                "classpath:key.txt");
    }

    private int readShiftAmount() {
        int shift = 1; //set default value
        try {
            File file = loadShiftAmount();
            Scanner sc = new Scanner(file);
            shift = sc.nextInt();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return shift;
    }
}
