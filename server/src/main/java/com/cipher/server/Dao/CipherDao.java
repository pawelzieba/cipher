package com.cipher.server.Dao;


import com.cipher.server.Entity.TextEntity;


public interface CipherDao {
    TextEntity encode(String text);
    TextEntity decode(String text);
}
