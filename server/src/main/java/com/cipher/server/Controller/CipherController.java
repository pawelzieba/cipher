package com.cipher.server.Controller;

import com.cipher.server.Service.CipherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/caesar")
public class CipherController {

    @Autowired
    private CipherService service;

    @RequestMapping(value = "/encode", method = RequestMethod.POST)
    public ResponseEntity<?> encode(@RequestParam(value = "encodeMessage") String text) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        return new ResponseEntity<>(service.encode(text), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/decode", method = RequestMethod.POST)
    public ResponseEntity<?> decode(@RequestParam(value = "decodeMessage") String text) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        return new ResponseEntity<>(service.decode(text), headers, HttpStatus.OK);
    }
}
