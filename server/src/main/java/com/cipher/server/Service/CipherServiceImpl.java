package com.cipher.server.Service;


import com.cipher.server.Dao.CipherDao;
import com.cipher.server.Entity.TextEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CipherServiceImpl implements CipherService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CipherServiceImpl.class);
    @Autowired
    private CipherDao cipher;

    public TextEntity encode(String s){
        LOGGER.info("service encode");
        return cipher.encode(s);
    }

    public TextEntity decode(String s){
        LOGGER.info("service decode");
        return cipher.decode(s);
    }
}
