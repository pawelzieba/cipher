package com.cipher.server.Service;


import com.cipher.server.Entity.TextEntity;

public interface CipherService {

    public TextEntity encode(String s);
    public TextEntity decode(String s);
}
