package com.cipher.server.Entity;



public class TextEntity {
    private String result;

    public TextEntity(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
